use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign,
};

use super::vector3::Vec3;

#[derive(Debug, PartialEq, Clone, Copy, Default)]
pub struct Matrix3D {
    pub n: [[f32; 3]; 3],
}

impl Matrix3D {
    pub fn new(
        n00: f32,
        n01: f32,
        n02: f32,
        n10: f32,
        n11: f32,
        n12: f32,
        n20: f32,
        n21: f32,
        n22: f32,
    ) -> Self {
        let mut n: [[f32; 3]; 3] = Default::default();

        n[0][0] = n00;
        n[0][1] = n10;
        n[0][2] = n20;

        n[1][0] = n01;
        n[1][1] = n11;
        n[1][2] = n21;

        n[2][0] = n02;
        n[2][1] = n12;
        n[2][2] = n22;

        Self { n }
    }

    pub fn from_vectors(a: &Vec3, b: &Vec3, c: &Vec3) -> Self {
        let mut n: [[f32; 3]; 3] = Default::default();

        n[0][0] = a.x;
        n[0][1] = a.y;
        n[0][2] = a.z;

        n[1][0] = b.x;
        n[1][1] = b.y;
        n[1][2] = b.z;

        n[2][0] = c.x;
        n[2][1] = c.y;
        n[2][2] = c.z;

        Self { n }
    }
}

impl Index<usize> for Matrix3D {
    type Output = Vec3;

    fn index(&self, j: usize) -> &Vec3 {
        match j {
            0 | 1 | 2 => unsafe { std::mem::transmute(&self.n[j]) },
            _ => panic!("index out of bounds: the len is 3 but the index is {}", j),
        }
    }
}

#[test]
fn index() {
    let m = Matrix3D::new(2., 3., 6., 12., 0.3, 2.3, 1., 9., 0.2);

    let v = m[0];

    assert_eq!(Vec3::new(2., 3., 6.), v);
}

impl IndexMut<usize> for Matrix3D {
    fn index_mut(&mut self, j: usize) -> &mut Vec3 {
        unsafe { std::mem::transmute(&mut self.n[j]) }
    }
}
